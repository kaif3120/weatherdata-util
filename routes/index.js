var express = require('express');
var router = express.Router();
var models = require('../models/index')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/syncDB', (req, res) => {
  if ((process.env.NODE_ENV || 'development') === 'development') {
    return models.sequelize.authenticate()
      .then(() => models.sequelize.query('SET FOREIGN_KEY_CHECKS = 0'))
      .then(() => models.sequelize.sync({
        force: true,
      }))
      .then(() => models.sequelize.query('SET FOREIGN_KEY_CHECKS = 1'))
      .then(() => {
        res.status(200).send('Synced Database')
      })
      .catch((err) => {
        res.status(500).send(err)
      })
  }
  res.send('Not allowed')
})




module.exports = router;

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var models = require('./models/index')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


const uploadFile = async () => {
  let jsonFile = null
  let arrWD = null
  let count = 0

  for (let i = 0; i <= 8332; i++) {

    jsonFile = require(`./weather/output/${i}.json`);
    arrWD = [];

    for (let outerKey in jsonFile['output']) {
      for (let innerKey in jsonFile['output'][outerKey]) {
        wd = {
          city: jsonFile['city'],
          lat: jsonFile['lat'],
          lon: jsonFile['lon'],
          alt: jsonFile['alt'],
          temp: outerKey,
          relHumidity: innerKey,
          nos_hours: jsonFile['output'][outerKey][innerKey]['nos_hours'],
          avg_due_point_temp: jsonFile['output'][outerKey][innerKey]['avg_due_point_temp'],
          avg_wet_bulb_temp: jsonFile['output'][outerKey][innerKey]['avg_wet_bulb_temp'],
          avg_mixing_ratio: jsonFile['output'][outerKey][innerKey]['avg_mixing_ratio']
        }
        arrWD.push(wd)
      }
    }
    count = count + arrWD.length
    await models.WeatherData.bulkCreate(arrWD)
    console.log(i + " has been saved")
  }
  console.log(count);
}

uploadFile();


module.exports = app;

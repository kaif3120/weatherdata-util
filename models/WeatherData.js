
module.exports = (sequelize, DataTypes) => {

    const WeatherData = sequelize.define('WeatherData', {

        id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            unique: 'weatherComposite'
        },
        city: {
            type: DataTypes.STRING,
            unique: 'weatherComposite'
        },
        lat: {
            type: DataTypes.FLOAT
        },
        lon: {
            type: DataTypes.FLOAT
        },
        alt: {
            type: DataTypes.FLOAT
        },
        temp: {
            type: DataTypes.FLOAT,
            unique: 'weatherComposite'
        },
        relHumidity:{
           type: DataTypes.FLOAT,
           unique: 'weatherComposite'
        },
        nos_hours:{
         type: DataTypes.FLOAT
        },
        avg_due_point_temp:{
        type: DataTypes.FLOAT
        },
        avg_wet_bulb_temp:{
        type: DataTypes.FLOAT
        },
        avg_mixing_ratio:{
        type: DataTypes.FLOAT
        },

    });

    return WeatherData;
}
